{
    "id": "4ffb9eba-100e-44c9-bcde-ce1437fb32b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_home_bedroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6eceafb5-28f8-49e1-8be8-c511b26ab7ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ffb9eba-100e-44c9-bcde-ce1437fb32b1",
            "compositeImage": {
                "id": "9fd5e408-54e6-4402-bf8b-91ae09bba7de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eceafb5-28f8-49e1-8be8-c511b26ab7ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fffeb276-c69f-4ab8-b902-3d3d310ca87f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eceafb5-28f8-49e1-8be8-c511b26ab7ee",
                    "LayerId": "8f7132a7-6881-4fd6-87c4-3d27e7ec90ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "8f7132a7-6881-4fd6-87c4-3d27e7ec90ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ffb9eba-100e-44c9-bcde-ce1437fb32b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "e18e8dd9-901e-4cfa-b5e1-b9ea55af9b38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_selectarrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb8010ae-a5a3-4817-a8a0-3755be52f0e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e18e8dd9-901e-4cfa-b5e1-b9ea55af9b38",
            "compositeImage": {
                "id": "9418064c-cae9-4155-a1c7-24e54b9a56e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb8010ae-a5a3-4817-a8a0-3755be52f0e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a018ac79-7b1c-4690-827a-8ab1b58817bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb8010ae-a5a3-4817-a8a0-3755be52f0e0",
                    "LayerId": "3a6f7775-566a-4272-8c9c-544e97052abb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "3a6f7775-566a-4272-8c9c-544e97052abb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e18e8dd9-901e-4cfa-b5e1-b9ea55af9b38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 0
}
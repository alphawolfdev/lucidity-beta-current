{
    "id": "ea0fad69-2050-4dd5-85f1-97934e6b47d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startmenubg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4282b1a-70e8-42a6-bdf7-5784bae98e97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea0fad69-2050-4dd5-85f1-97934e6b47d4",
            "compositeImage": {
                "id": "57bdbbd5-1632-48b9-aa0d-230c03c9f6ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4282b1a-70e8-42a6-bdf7-5784bae98e97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2d64ccf-63e7-489b-bef8-13af9d68fb28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4282b1a-70e8-42a6-bdf7-5784bae98e97",
                    "LayerId": "5f2ce4dd-6447-4b53-b0ba-db468738d6db"
                },
                {
                    "id": "e80119aa-3565-4f4a-a0f9-9b6f91910d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4282b1a-70e8-42a6-bdf7-5784bae98e97",
                    "LayerId": "2c876cc3-d133-40fc-a4a2-3dddc8165023"
                },
                {
                    "id": "df87de5d-5879-4d1d-848d-725d90ae0257",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4282b1a-70e8-42a6-bdf7-5784bae98e97",
                    "LayerId": "66ba8df8-4f3f-48e7-a0bd-ea84ea802423"
                },
                {
                    "id": "bae603e6-9acf-4ae1-ab36-593ad738c483",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4282b1a-70e8-42a6-bdf7-5784bae98e97",
                    "LayerId": "a48ce05a-6e64-48e5-860f-5c28f4a1ce85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "2c876cc3-d133-40fc-a4a2-3dddc8165023",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea0fad69-2050-4dd5-85f1-97934e6b47d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Title",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a48ce05a-6e64-48e5-860f-5c28f4a1ce85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea0fad69-2050-4dd5-85f1-97934e6b47d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Crystals",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5f2ce4dd-6447-4b53-b0ba-db468738d6db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea0fad69-2050-4dd5-85f1-97934e6b47d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "66ba8df8-4f3f-48e7-a0bd-ea84ea802423",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea0fad69-2050-4dd5-85f1-97934e6b47d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Sky",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 168,
    "yorig": 440
}
{
    "id": "23bf4186-1730-49aa-bbf5-3b21143193f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36c946c0-b0f6-4c7c-96b1-c3b6b1375d63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23bf4186-1730-49aa-bbf5-3b21143193f4",
            "compositeImage": {
                "id": "98fe366c-9d71-40c6-8054-5c26646b88a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36c946c0-b0f6-4c7c-96b1-c3b6b1375d63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fdfb5f9-119d-483a-95d3-8025245f22cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36c946c0-b0f6-4c7c-96b1-c3b6b1375d63",
                    "LayerId": "c94fca20-69e7-4753-a0c1-887897a26fd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c94fca20-69e7-4753-a0c1-887897a26fd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23bf4186-1730-49aa-bbf5-3b21143193f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
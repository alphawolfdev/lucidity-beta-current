{
    "id": "2203c51c-3cfc-40cf-8b19-90d7839c0f87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fade640",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9187a3eb-3368-4dd0-a60f-94e17564bffa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203c51c-3cfc-40cf-8b19-90d7839c0f87",
            "compositeImage": {
                "id": "884a8667-d149-48ea-b7a1-6de7313934d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9187a3eb-3368-4dd0-a60f-94e17564bffa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7dcb9a8-b204-4de4-ad83-a5cecbd2a5b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9187a3eb-3368-4dd0-a60f-94e17564bffa",
                    "LayerId": "d9e27df4-18c8-4b3a-81cc-875329f4dace"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "d9e27df4-18c8-4b3a-81cc-875329f4dace",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2203c51c-3cfc-40cf-8b19-90d7839c0f87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "ebba89fd-90f0-45ff-aac0-150e8960638e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_selectcross",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5d02049-73ac-4a5c-92b1-983dc4ba10ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebba89fd-90f0-45ff-aac0-150e8960638e",
            "compositeImage": {
                "id": "ce26de66-0332-42ae-ad08-155eaaa3266b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5d02049-73ac-4a5c-92b1-983dc4ba10ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37b46bc2-92da-417a-960a-1a5305774c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5d02049-73ac-4a5c-92b1-983dc4ba10ea",
                    "LayerId": "86e4ce71-f4ce-4e6b-a9fc-5c75be9af100"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "86e4ce71-f4ce-4e6b-a9fc-5c75be9af100",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebba89fd-90f0-45ff-aac0-150e8960638e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}
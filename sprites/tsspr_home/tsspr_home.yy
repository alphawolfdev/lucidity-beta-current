{
    "id": "9088edd1-244d-484b-aa19-00f0d2b65e04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tsspr_home",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 32,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8477d410-8d26-4a0a-b6fb-cac1728ce2a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9088edd1-244d-484b-aa19-00f0d2b65e04",
            "compositeImage": {
                "id": "0629b7b2-72f0-4d3e-ac2c-a90c7ac9a3c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8477d410-8d26-4a0a-b6fb-cac1728ce2a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96f33f3c-74bf-4efd-82d8-c67289bb6363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8477d410-8d26-4a0a-b6fb-cac1728ce2a7",
                    "LayerId": "fba6fbb0-38d5-4f3e-92d5-2738d515daaa"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 160,
    "layers": [
        {
            "id": "fba6fbb0-38d5-4f3e-92d5-2738d515daaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9088edd1-244d-484b-aa19-00f0d2b65e04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": -4,
    "yorig": 6
}
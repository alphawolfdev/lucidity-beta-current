{
    "id": "84d30358-9e0a-4fb8-b2eb-7008a53cf902",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_invis",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be237142-c1e3-4f70-89d5-3bf4733a50f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d30358-9e0a-4fb8-b2eb-7008a53cf902",
            "compositeImage": {
                "id": "38a0b95e-c7d2-41c6-a5d7-6685c8aca734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be237142-c1e3-4f70-89d5-3bf4733a50f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c77881c2-b598-4498-ad18-e34b38e861ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be237142-c1e3-4f70-89d5-3bf4733a50f1",
                    "LayerId": "68e343f5-6c55-4fea-85b7-9ecc47f0e6a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "68e343f5-6c55-4fea-85b7-9ecc47f0e6a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84d30358-9e0a-4fb8-b2eb-7008a53cf902",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
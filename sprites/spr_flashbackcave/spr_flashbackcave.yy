{
    "id": "57fdceac-a359-4957-986f-f37630036625",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flashbackcave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1caaa484-ce86-492f-b38a-1a839cbc8300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57fdceac-a359-4957-986f-f37630036625",
            "compositeImage": {
                "id": "eb2c3c2a-a778-4838-b2e5-0c2d90c15f56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1caaa484-ce86-492f-b38a-1a839cbc8300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e1ca2e3-13ea-4202-8548-d50a0a2ab82f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1caaa484-ce86-492f-b38a-1a839cbc8300",
                    "LayerId": "f2d6ff00-0552-4200-9b87-50d0a323febc"
                },
                {
                    "id": "cd1d1d02-bea2-41b3-a0b5-4860df8cd5b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1caaa484-ce86-492f-b38a-1a839cbc8300",
                    "LayerId": "fb58dc89-c1f2-4eb4-af49-fbd8c106faac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "__type": "GMImageFolderLayer_Model:#YoYoStudio.MVCFormat",
            "id": "c1aa0835-fa1d-414d-aaa4-a5beea441b8f",
            "modelName": "GMImageFolderLayer",
            "mvc": "1.0",
            "SpriteId": "57fdceac-a359-4957-986f-f37630036625",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer Group 1",
            "opacity": 100,
            "visible": true,
            "layers": [
                {
                    "id": "f2d6ff00-0552-4200-9b87-50d0a323febc",
                    "modelName": "GMImageLayer",
                    "mvc": "1.0",
                    "SpriteId": "57fdceac-a359-4957-986f-f37630036625",
                    "blendMode": 0,
                    "isLocked": false,
                    "name": "default",
                    "opacity": 100,
                    "visible": true
                }
            ]
        },
        {
            "id": "fb58dc89-c1f2-4eb4-af49-fbd8c106faac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57fdceac-a359-4957-986f-f37630036625",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}
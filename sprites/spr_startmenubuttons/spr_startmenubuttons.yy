{
    "id": "5aa7b4d0-5a1e-45d5-9a04-e0ecb08aeff6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startmenubuttons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 3,
    "bbox_right": 75,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2676b30-38c8-4cbb-a370-60ffdc2495cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aa7b4d0-5a1e-45d5-9a04-e0ecb08aeff6",
            "compositeImage": {
                "id": "3447fddc-3d14-4806-aede-1e2b6a878fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2676b30-38c8-4cbb-a370-60ffdc2495cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7210d7a-0717-4bae-8d71-7d26b2731be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2676b30-38c8-4cbb-a370-60ffdc2495cf",
                    "LayerId": "075610c0-5fb4-465b-a72e-81053586c519"
                }
            ]
        },
        {
            "id": "36eb7b73-2dda-4b0d-88d3-81fd51a4dfcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aa7b4d0-5a1e-45d5-9a04-e0ecb08aeff6",
            "compositeImage": {
                "id": "d5793407-34df-46d7-b886-8e339f8bc311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36eb7b73-2dda-4b0d-88d3-81fd51a4dfcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f251ebc-25dd-4e6a-90ca-f18ae67e178c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36eb7b73-2dda-4b0d-88d3-81fd51a4dfcb",
                    "LayerId": "075610c0-5fb4-465b-a72e-81053586c519"
                }
            ]
        },
        {
            "id": "933ac63d-187f-4832-803e-6b2f4bdcdd29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aa7b4d0-5a1e-45d5-9a04-e0ecb08aeff6",
            "compositeImage": {
                "id": "38b1672a-32d7-4051-9d43-adf530e3e6ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933ac63d-187f-4832-803e-6b2f4bdcdd29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47ca1515-818f-4b0c-bcea-78601da71495",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933ac63d-187f-4832-803e-6b2f4bdcdd29",
                    "LayerId": "075610c0-5fb4-465b-a72e-81053586c519"
                }
            ]
        },
        {
            "id": "7fd5a68e-cb82-40f7-8334-73a7a8979ee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aa7b4d0-5a1e-45d5-9a04-e0ecb08aeff6",
            "compositeImage": {
                "id": "c674fbb5-ed5a-4747-ad3e-5e301f189b20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fd5a68e-cb82-40f7-8334-73a7a8979ee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "227e2458-7136-40d3-b65b-2ba038157f69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fd5a68e-cb82-40f7-8334-73a7a8979ee7",
                    "LayerId": "075610c0-5fb4-465b-a72e-81053586c519"
                }
            ]
        },
        {
            "id": "c54b8872-ae98-4342-8fb7-68962458f817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aa7b4d0-5a1e-45d5-9a04-e0ecb08aeff6",
            "compositeImage": {
                "id": "be8d8fd6-a848-4cd6-852e-cfa96db24079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c54b8872-ae98-4342-8fb7-68962458f817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ee4e23-17a2-442e-a6e4-39ed5e7b9c3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c54b8872-ae98-4342-8fb7-68962458f817",
                    "LayerId": "075610c0-5fb4-465b-a72e-81053586c519"
                }
            ]
        },
        {
            "id": "e9a00eb1-f23b-4489-9042-6a4132df1e22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aa7b4d0-5a1e-45d5-9a04-e0ecb08aeff6",
            "compositeImage": {
                "id": "57e6b247-7a7b-422e-a90f-04b2feaf6f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9a00eb1-f23b-4489-9042-6a4132df1e22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0c3d16-923e-4307-ab69-8ab5d05453ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9a00eb1-f23b-4489-9042-6a4132df1e22",
                    "LayerId": "075610c0-5fb4-465b-a72e-81053586c519"
                }
            ]
        },
        {
            "id": "7cf81213-6d58-4249-9db0-0be0da61ecb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aa7b4d0-5a1e-45d5-9a04-e0ecb08aeff6",
            "compositeImage": {
                "id": "82d07fc8-41df-4e85-b16d-d24872c9830c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf81213-6d58-4249-9db0-0be0da61ecb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e62ed615-df0a-4041-b537-eb0d379a672d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf81213-6d58-4249-9db0-0be0da61ecb7",
                    "LayerId": "075610c0-5fb4-465b-a72e-81053586c519"
                }
            ]
        },
        {
            "id": "038d1fce-41bf-470a-941c-3cd21d7f497e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aa7b4d0-5a1e-45d5-9a04-e0ecb08aeff6",
            "compositeImage": {
                "id": "654abf11-21b7-47e1-9549-35249c9b6b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "038d1fce-41bf-470a-941c-3cd21d7f497e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5100056-d589-46b6-9361-489bd709dcbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "038d1fce-41bf-470a-941c-3cd21d7f497e",
                    "LayerId": "075610c0-5fb4-465b-a72e-81053586c519"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "075610c0-5fb4-465b-a72e-81053586c519",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5aa7b4d0-5a1e-45d5-9a04-e0ecb08aeff6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 20
}
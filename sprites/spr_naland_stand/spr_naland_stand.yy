{
    "id": "d1a3762c-a2bc-4105-aed6-63b281a1f864",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_naland_stand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 4,
    "bbox_right": 73,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77030263-3f8b-44e0-8cff-c09acc686567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1a3762c-a2bc-4105-aed6-63b281a1f864",
            "compositeImage": {
                "id": "f522da2a-fd5a-4717-8984-d60fffbe6c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77030263-3f8b-44e0-8cff-c09acc686567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bebdc849-5e66-4aae-b92f-939474213151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77030263-3f8b-44e0-8cff-c09acc686567",
                    "LayerId": "08ef7a99-9d04-4aff-aa3a-1a230533a95a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 159,
    "layers": [
        {
            "id": "08ef7a99-9d04-4aff-aa3a-1a230533a95a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1a3762c-a2bc-4105-aed6-63b281a1f864",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 94,
    "yorig": 37
}
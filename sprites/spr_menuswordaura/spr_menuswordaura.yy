{
    "id": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menuswordaura",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75388103-123f-4e8e-bd3a-e133d4ac9116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "087b75fc-f280-4194-976b-86c9672206cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75388103-123f-4e8e-bd3a-e133d4ac9116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1729e4c-244b-45ac-87f9-2d9bb0fadaa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75388103-123f-4e8e-bd3a-e133d4ac9116",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        },
        {
            "id": "9708e1a4-7ae0-49a5-8e99-4a930e49518c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "4ae2bb43-47d5-4bef-ad0d-c7f48a4b9141",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9708e1a4-7ae0-49a5-8e99-4a930e49518c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce941ba8-065e-40ec-80bd-d5c7c32b348b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9708e1a4-7ae0-49a5-8e99-4a930e49518c",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        },
        {
            "id": "df477086-54cf-4609-a5bc-94a7b8304d51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "9cb44cf6-5ebc-4fab-8e33-49f1228881f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df477086-54cf-4609-a5bc-94a7b8304d51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e46f2ff1-8aa6-47df-a1f1-d656b98cfa38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df477086-54cf-4609-a5bc-94a7b8304d51",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        },
        {
            "id": "594b76c6-c807-441a-8098-2ed3c20cafa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "7dcae599-4333-4222-a78c-b3630caeef4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "594b76c6-c807-441a-8098-2ed3c20cafa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7e0b24-6cf6-46ec-8d9a-2e7d6921ed67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "594b76c6-c807-441a-8098-2ed3c20cafa7",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        },
        {
            "id": "cf29980a-53ce-4dbf-b0e0-7d1b94cf827f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "807c960b-3b0f-4113-8f99-069a90f995a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf29980a-53ce-4dbf-b0e0-7d1b94cf827f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50edd49b-a37f-45c9-87c0-73fe59cc210a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf29980a-53ce-4dbf-b0e0-7d1b94cf827f",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        },
        {
            "id": "59532639-b057-41f7-b828-5d6179b3209d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "a960257c-643a-4e74-97d9-91f38203dc44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59532639-b057-41f7-b828-5d6179b3209d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8efb11b6-db2b-4755-a7a2-b496d6591f07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59532639-b057-41f7-b828-5d6179b3209d",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        },
        {
            "id": "97dd00b1-5ea8-4745-9624-ef5640a83f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "4f5cb0d0-6f85-436d-8c4d-cab097b88570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97dd00b1-5ea8-4745-9624-ef5640a83f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02cac313-3443-4174-9bfa-71e0602d9613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97dd00b1-5ea8-4745-9624-ef5640a83f24",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        },
        {
            "id": "e8b717eb-735b-406b-90ea-3f8d114b9372",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "7b72711e-4e35-4a88-9750-ce9f98871f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b717eb-735b-406b-90ea-3f8d114b9372",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "675f45fd-e751-485d-aa9b-035ed0f2a01c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b717eb-735b-406b-90ea-3f8d114b9372",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        },
        {
            "id": "7366e033-6f15-43b3-a047-0a39629c103b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "eaf49f95-809a-4dc2-9c96-6066841939e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7366e033-6f15-43b3-a047-0a39629c103b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d8f50b0-06b9-4f60-9279-fb0d05cfb5ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7366e033-6f15-43b3-a047-0a39629c103b",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        },
        {
            "id": "bee6e868-96c4-410e-bd3c-de868a764a70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "3f4cb26e-e503-4bdd-82a3-770bffa80898",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bee6e868-96c4-410e-bd3c-de868a764a70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "123764b0-eab1-47e6-9beb-d6088cb49eee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bee6e868-96c4-410e-bd3c-de868a764a70",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        },
        {
            "id": "cd358762-4589-42eb-b0ed-c84003a0db23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "compositeImage": {
                "id": "4f1fdc91-6dd6-4a05-a6c6-1ff8203335e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd358762-4589-42eb-b0ed-c84003a0db23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d31ffd2-75a6-48bb-b16a-797d90adf699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd358762-4589-42eb-b0ed-c84003a0db23",
                    "LayerId": "ea266e31-b019-4905-985f-c753b1dc1560"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "ea266e31-b019-4905-985f-c753b1dc1560",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 0,
    "yorig": 0
}
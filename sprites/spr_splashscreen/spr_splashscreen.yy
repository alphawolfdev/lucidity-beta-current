{
    "id": "b44f7d95-5e5f-4a6a-b090-6c5c8b7ec49a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_splashscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "582259ef-94e3-41f8-b6f0-77718564e80e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b44f7d95-5e5f-4a6a-b090-6c5c8b7ec49a",
            "compositeImage": {
                "id": "6d4c9e27-c134-4abc-a137-c5e35ff2f707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "582259ef-94e3-41f8-b6f0-77718564e80e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e50f19d-2d1a-486b-9de1-45f60cd1c985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "582259ef-94e3-41f8-b6f0-77718564e80e",
                    "LayerId": "b2e09bab-1ae3-4f8b-a866-0404bafa1077"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "b2e09bab-1ae3-4f8b-a866-0404bafa1077",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b44f7d95-5e5f-4a6a-b090-6c5c8b7ec49a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 147,
    "yorig": 53
}
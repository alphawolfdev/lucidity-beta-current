{
    "id": "71bf9f9d-769a-429a-99b0-8fd19b9386a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spacebar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a29bc0c-b8bf-4dd0-a967-31262f578515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71bf9f9d-769a-429a-99b0-8fd19b9386a5",
            "compositeImage": {
                "id": "22a3bf28-247c-4d2f-a104-7f2a7cac9a65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a29bc0c-b8bf-4dd0-a967-31262f578515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87d5b129-bee5-4eea-86a0-45fc328c6d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a29bc0c-b8bf-4dd0-a967-31262f578515",
                    "LayerId": "9f04bb9e-0387-4e2f-8ac3-45cfb014272f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9f04bb9e-0387-4e2f-8ac3-45cfb014272f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71bf9f9d-769a-429a-99b0-8fd19b9386a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
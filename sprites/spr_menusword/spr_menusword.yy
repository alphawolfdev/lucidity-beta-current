{
    "id": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menusword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "130dc5f9-1c5e-4dee-ad55-ca65a00decdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "7480044b-8f3b-4ee2-9156-bc73d55ca3c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "130dc5f9-1c5e-4dee-ad55-ca65a00decdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "043fd7c4-c0ba-42df-8eaf-c8022285ed31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "130dc5f9-1c5e-4dee-ad55-ca65a00decdd",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "7e6a2908-6e4b-4783-9827-db184b048161",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "315ec683-ef43-4345-8296-65e073fef198",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e6a2908-6e4b-4783-9827-db184b048161",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee026ec4-dc0d-45c3-896c-075c7a2cb6a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e6a2908-6e4b-4783-9827-db184b048161",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "5bd73cf3-ed39-4921-8a12-0581a25c9b3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "fa39bd34-75a4-49c3-8d03-e6e5d24d1138",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bd73cf3-ed39-4921-8a12-0581a25c9b3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01dbd12f-f519-4b9a-9298-f033acc8388c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bd73cf3-ed39-4921-8a12-0581a25c9b3d",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "8be7d477-a6c8-40ea-83b9-5026c65683e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "db967344-7322-4bb3-8fd9-961224b1a8ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8be7d477-a6c8-40ea-83b9-5026c65683e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08942a6a-1ced-4f25-9f62-e31a11cdf720",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8be7d477-a6c8-40ea-83b9-5026c65683e8",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "86f15aea-8af8-41ca-b948-05b37aca62f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "f74f0fc4-6bfb-46d9-a9ff-2f00419dfac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f15aea-8af8-41ca-b948-05b37aca62f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cefb0406-13ef-4ccc-949a-fa1f38ece2bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f15aea-8af8-41ca-b948-05b37aca62f5",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "6473d0be-601f-4e01-ad33-0d865517c1e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "fef5fe44-d0ef-4589-ac8e-d377ae3423c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6473d0be-601f-4e01-ad33-0d865517c1e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9736d204-b46b-48cc-aef2-e1f3562b72f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6473d0be-601f-4e01-ad33-0d865517c1e9",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "5b178a76-7343-421c-8411-0520d3fdfeb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "af84ab46-8a84-451c-a423-3b2717ea0ce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b178a76-7343-421c-8411-0520d3fdfeb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1019407-7f0f-407d-a7e8-3fc7df1c014f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b178a76-7343-421c-8411-0520d3fdfeb9",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "a1cd5bc0-3c87-44f8-9a94-2edc955554be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "832c7420-5bea-47d7-b59a-4cc9011dc93c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1cd5bc0-3c87-44f8-9a94-2edc955554be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e22b5ea1-961e-41e6-845e-0d18a59df6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1cd5bc0-3c87-44f8-9a94-2edc955554be",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "e844b503-1aff-44ea-b3a0-1ec6862058d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "74fc3735-febb-444d-8376-55ae8e3e7e61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e844b503-1aff-44ea-b3a0-1ec6862058d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e5d16fd-6194-419e-a06f-bc7e8e7e438d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e844b503-1aff-44ea-b3a0-1ec6862058d7",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "7ec48fe3-0595-4e23-a719-4fe5e02e758a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "f43e1609-19d1-424e-8d53-006c17ee9c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ec48fe3-0595-4e23-a719-4fe5e02e758a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "501db21e-e365-418d-877a-b22aa123dcbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ec48fe3-0595-4e23-a719-4fe5e02e758a",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "773a1994-4a11-44b2-942a-94dcbc053c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "a0ce5605-5eaf-44f0-926c-e45b3c3c4912",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "773a1994-4a11-44b2-942a-94dcbc053c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e72544-5b9b-41a9-87d5-894281d97a7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "773a1994-4a11-44b2-942a-94dcbc053c5b",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "15687ed5-6611-45e4-8acb-122eb1cb81e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "a3d5db64-4d0c-4108-8e17-411f0318bf7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15687ed5-6611-45e4-8acb-122eb1cb81e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "121b82a6-ac42-480c-a8e0-327a51825333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15687ed5-6611-45e4-8acb-122eb1cb81e9",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "747c32ed-fcc6-4951-bced-077544312faf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "44008780-35e0-4cdd-8470-8a2831e4d0a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "747c32ed-fcc6-4951-bced-077544312faf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c639d423-7dbe-4f93-a677-828ca006bf9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "747c32ed-fcc6-4951-bced-077544312faf",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "3460d3c7-14a6-47ed-ac37-6f994d5ac6be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "b2c5e4c2-bdb5-41c8-8f8b-0a83322bbbad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3460d3c7-14a6-47ed-ac37-6f994d5ac6be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6687bc8-18a8-4986-b04e-e58c835f3739",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3460d3c7-14a6-47ed-ac37-6f994d5ac6be",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "d18975ea-1b0b-4d6f-a03b-15d89bfb161b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "50670d6e-cf95-4213-b800-fa61169ef132",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d18975ea-1b0b-4d6f-a03b-15d89bfb161b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41c7b244-b136-4e32-8f91-a5d104db810f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d18975ea-1b0b-4d6f-a03b-15d89bfb161b",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "2b4ab579-26c2-4a93-bca7-0c67f83c696c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "dd481e6e-da11-490f-9a91-ce230a031fd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b4ab579-26c2-4a93-bca7-0c67f83c696c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a7c2325-c91e-407d-81e9-169a16c3eb0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b4ab579-26c2-4a93-bca7-0c67f83c696c",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "635bc031-5974-4d5b-b85c-1b53f7aa8819",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "e3dd2a21-b013-41ae-bba5-2a64fb9927e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "635bc031-5974-4d5b-b85c-1b53f7aa8819",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed2aba9d-600e-478f-8243-82aeb1948659",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "635bc031-5974-4d5b-b85c-1b53f7aa8819",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "6a8a3d16-02b0-444e-b6cd-6f35092942b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "c9b0abd9-39a1-465a-81a3-a208f7bac771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a8a3d16-02b0-444e-b6cd-6f35092942b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f11f33a-de41-45dd-b064-9685329ac9ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a8a3d16-02b0-444e-b6cd-6f35092942b0",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "976b6916-80d2-4e78-b9e1-424843ba8a58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "d8f7ef51-a56d-49e8-b986-19d314c1beaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "976b6916-80d2-4e78-b9e1-424843ba8a58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e977ab74-2b20-434c-8946-0cacc8ecd101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "976b6916-80d2-4e78-b9e1-424843ba8a58",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "478f8c78-d6d4-46da-94af-ea206960f74b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "b2d2c1db-ad04-4f39-ac0b-1a60f15de007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "478f8c78-d6d4-46da-94af-ea206960f74b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0a723e6-9e8f-4b27-9f8e-fe970c5158f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "478f8c78-d6d4-46da-94af-ea206960f74b",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        },
        {
            "id": "9ef21aa5-502b-432a-9b85-2faf0f7238b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "compositeImage": {
                "id": "16645f01-2126-4fdc-b381-0b9117bf664a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef21aa5-502b-432a-9b85-2faf0f7238b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0c3550f-f21f-49f1-a154-7e2939b23b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef21aa5-502b-432a-9b85-2faf0f7238b9",
                    "LayerId": "95bd047b-1a02-4d41-8026-131cbdede8f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "95bd047b-1a02-4d41-8026-131cbdede8f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "60d29c91-348f-4c76-a531-29348492b93e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72f53a69-86c9-44f2-873f-c79b5c23d0e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d29c91-348f-4c76-a531-29348492b93e",
            "compositeImage": {
                "id": "5e5e73d6-d8d8-4e07-a531-d55f0b1e3e07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72f53a69-86c9-44f2-873f-c79b5c23d0e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc9bfba3-b6f4-4b68-a651-17c0d14a68ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72f53a69-86c9-44f2-873f-c79b5c23d0e6",
                    "LayerId": "13d1d725-73b4-4f91-9ab1-64105db48baa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "13d1d725-73b4-4f91-9ab1-64105db48baa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60d29c91-348f-4c76-a531-29348492b93e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": -14,
    "yorig": 5
}
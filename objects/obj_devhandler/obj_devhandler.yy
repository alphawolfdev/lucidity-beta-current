{
    "id": "0f1bef29-0dd4-48dc-a8f7-8aaabcb81500",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_devhandler",
    "eventList": [
        {
            "id": "bb71b674-90f2-46e5-a099-51817ae8475a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f1bef29-0dd4-48dc-a8f7-8aaabcb81500"
        },
        {
            "id": "318a6429-211d-46c6-ade9-ed3ca126ea73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "0f1bef29-0dd4-48dc-a8f7-8aaabcb81500"
        },
        {
            "id": "e65dd3db-6400-46e5-b0cc-4273d0404675",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0f1bef29-0dd4-48dc-a8f7-8aaabcb81500"
        },
        {
            "id": "7907cba8-ff77-43e0-a2a7-dddf0df21904",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0f1bef29-0dd4-48dc-a8f7-8aaabcb81500"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
/// @Sets up vars
//COLOURS AND COMMANDS
//   /c = colour, w=white,r=red,b=blue,y=yellow etc.
//  /w = wait, number after is interval
// /e = end of sentence.
// /s = type speed, f=fast,t=veryfast,=s=slow,m=medium
// /n sets all prefaces to normal
// You can write your code in this editor
letterinit = false;
throughloop = false;
braked = false;
sound = snd_click;
msgdone = false;
emit = audio_emitter_create();
gain = 1;
audio_emitter_gain(emit,gain);
audio_play_sound_on(emit,mus_story_music,true,0);
alpha = 1;
sndwait = 0;
global.wait = 0;
isEscape = false;
message[0] = "/n Life./w4  Such a simple word, yet in it, is our existence. Our very being.  /cbYOUR/cw  very being./w5 ";
message[1] = "/n Our planet, Earth, is the basis for life. And on it, life itself is divided into  /cb11 /cw  sections which work together to keep someone ALIVE./w5 ";
message[2] = "/n Strength. Awareness. Nature. Creation. Sense. Protection. Soul. Time. Memory. Truth. Reality./w5 ";
message[3] = "/n However, most humans aren't aware of this. They are oblivious to the innermost workings of this planet, this universe. They are asleep./w5 ";
message[4] = "/n But... There are those who have opened their eyes, and as a result are able to tap in to this power, some with more skill than others. The/cb  LUCID/cw . At first, this was good./w5 ";
message[5] = "/n Unfortunately, as is human nature, these individuals became dissatisfied with the status quo./w5 ";
message[6] = "/n Something is stirring.../w5 ";
message[7] = "/n It is your turn to awake now. You are needed./w5 ";
message[8] = "/n Awaken to your responsibility. /ss Your /cb identity.../w5 ";
wait = 0;
addedx = 0;

addedy = 0;
linespace = 40;
timesadded = 0;
message_max = 8;
letterchars[0] = "";
message_current = 0;
message_length = string_length(message[message_current]);
fastspeed = 1.2/1.72;
veryfastspeed = 1.7/1.72;
mediumspeed = 0.54/1.72;
slowspeed = 0.35/1.72;
line = 0;
font = fnt_bigtext;
colour = c_white;
waited = false;
letter = "";
character[0] = "";
letterid = 0;
currentspeed = mediumspeed;
letterx[0] = room_width/8;
lettery[0] = room_height/8;
letterstartx = room_width/8;
letterstarty = room_height/8;
maxx = room_width/8;
maxx *= 6.5;
spacesize = 16;
{
    "id": "c8efe9d4-de8b-4d49-9868-dbd094135f87",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_legend_tell",
    "eventList": [
        {
            "id": "cdb359b0-d0e7-43a0-a90a-178f9f37c836",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c8efe9d4-de8b-4d49-9868-dbd094135f87"
        },
        {
            "id": "c063ae78-43b1-44dd-84ad-4b053e084a35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c8efe9d4-de8b-4d49-9868-dbd094135f87"
        },
        {
            "id": "7539b0b2-1fb2-4fc8-a19b-ae26c7a4144f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c8efe9d4-de8b-4d49-9868-dbd094135f87"
        },
        {
            "id": "f6e4cf64-06fc-4586-afa4-b925aa6695fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "c8efe9d4-de8b-4d49-9868-dbd094135f87"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
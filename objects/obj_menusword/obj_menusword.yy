{
    "id": "1e775daa-50ae-4085-8e7e-6d2b5442b4b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menusword",
    "eventList": [
        {
            "id": "b0533cb2-ed8f-4fcb-b9d9-2d3340191a61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e775daa-50ae-4085-8e7e-6d2b5442b4b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7aa8cf1-5a4b-486f-9dcf-a52003ac8ac8",
    "visible": true
}
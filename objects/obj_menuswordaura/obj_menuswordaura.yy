{
    "id": "2f02abc6-ad16-4508-b43e-a6879d7b2aca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menuswordaura",
    "eventList": [
        {
            "id": "6bc8d0de-b511-4e11-bea4-2fe22148ee8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f02abc6-ad16-4508-b43e-a6879d7b2aca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7d05bfae-eea0-4d20-a9ee-d77f2031b299",
    "visible": true
}
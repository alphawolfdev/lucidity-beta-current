{
    "id": "adb9c053-3faf-494a-9bfb-735808922eea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_uiball",
    "eventList": [
        {
            "id": "8ffaf319-bc34-4a72-a481-9743e61c61ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "adb9c053-3faf-494a-9bfb-735808922eea"
        },
        {
            "id": "1ea46938-40fb-4c53-8e2b-bff4c261bd53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "adb9c053-3faf-494a-9bfb-735808922eea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "23bf4186-1730-49aa-bbf5-3b21143193f4",
    "visible": true
}
{
    "id": "2973b35f-9a5f-4b7d-8087-46ccc8f37a5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_showSplash",
    "eventList": [
        {
            "id": "e7a3dac2-4b0f-4bbb-8f55-cf15f8e2fecd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2973b35f-9a5f-4b7d-8087-46ccc8f37a5b"
        },
        {
            "id": "ebf0b583-677f-4896-ba1e-2e07b00c86b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2973b35f-9a5f-4b7d-8087-46ccc8f37a5b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b44f7d95-5e5f-4a6a-b090-6c5c8b7ec49a",
    "visible": true
}
/// @description Insert description here
// You can write your code in this editor

draw_set_colour(c_white);
draw_set_font(fnt_bigtext);
mepix = string_width(name);	

if(name_entered == false)
{
	draw_set_alpha(alpha);
	draw_text_ext(40,20,question[currquestion],40,600);
currx = 100;
maxx = 580;
curry = 320;
letx = 100;
lety = 320;
for(f=0;f<possible_letters;f++)
{
	
	letx += random_range(-2,+2);
	lety += random_range(-2,+2);
	draw_set_font(fnt_nameletters);
	if(f == selected-1)
	{
		col = c_aqua;
	}else
	{
	col = c_white;	
	}
	if(selected >= 27)
	{
	col = c_white;	
	}
	if(!capital)
	{
	letterdraw = letter[f];
	}else
	{
	letterdraw = string_upper(letter[f]);	
	}
	
	draw_text_colour(letx,lety,letterdraw,col,col,col,col,alpha)
	if(currx < maxx-60)
	{
		currx += space_inbetween;
	}else
	{
	currx = 100;
	curry += 40;
	}
	letx = currx;
	lety = curry;
	
	
}
draw_text_colour(100,420,"BACKSPACE",backspacecolour,backspacecolour,backspacecolour,backspacecolour,alpha);
	draw_text_colour(420,420,"CONFIRM",confirmcolour,confirmcolour,confirmcolour,confirmcolour,alpha);
draw_sprite_ext(spr_spacebar,0,currx+space_inbetween-5+random_range(-2,2),curry+7+random_range(-2,2),2,2,0,spacecolour,alpha);
draw_sprite_ext(spr_uparrow,0,20+random_range(2,-2),355+ random_range(-2,2),3,3,0,upcolour,alpha);
if(name_entered == false)
{
if(name == "")
{
	if(seedash == true)
	{
draw_text_transformed(90,160,"|",1.4,1.4,0);
	}
}else
{
if(seedash == true)
	{
		
draw_text_transformed(90,160,name+"|",1.4,1.4,0);
	}else
	{
	draw_text_transformed_colour(90+nameextrax,160,name,1.4+extranamesize,1.4+extranamesize,namerotation,c_white,c_white,c_white,c_white,1);	
	}
}
}
}
if(can_confirm == true)
{
	draw_set_alpha(confirmalpha);
	draw_text_ext(40,20,"Are you sure about this? You're Happy with this name?",40,600);
	draw_set_font(fnt_nameletters);
	draw_text_transformed_colour(90+nameextrax,160,name,1.4+extranamesize,1.4+extranamesize,namerotation,c_white,c_white,c_white,c_white,1);
	draw_set_font(fnt_bigtext);
	option_inbetween = 0;
	for(l=0;l<2;l++)
	{

	if(l == option_selected)
	{
		selectcolour = c_aqua;
	}else
	{
	selectcolour = c_white;	
	}
	draw_text_colour(40+option_inbetween,380,option[l],selectcolour,selectcolour,selectcolour,selectcolour,1);
	option_inbetween += 480;
	}
}
if(show_response == true)
{
	draw_set_alpha(responsealpha);
	draw_text_ext(40,20,response,40,600);
	draw_set_font(fnt_nameletters);
	draw_text_transformed_colour(90+nameextrax,180,name,1.4+extranamesize,1.4+extranamesize,namerotation,c_white,c_white,c_white,c_white,1);
	draw_set_font(fnt_bigtext);
	
	draw_text_colour(room_width/2-(0.5*string_width("Understood")),380,"Understood",c_aqua,c_aqua,c_aqua,c_aqua,responsealpha);
	option_inbetween += 480;
	}
	
if(entercharattributes[currattribute])
{
	if(waveaway == true)
	{
	shader_set(shd_increasingwave);	
	shader_set_uniform_f(waveTime,timepassed);
	}
	draw_set_alpha(attributealpha[currattribute]);
	draw_set_font(fnt_bigtext);
	draw_text_ext(40,20,attributequestion[currattribute],40,600);
	for(p=0;p<5;p++)
	{
		if(p == selectedattribute)
		{
		atrcolour = c_aqua;	
		}else
		{
		atrcolour =c_white;	
		}
		draw_text_transformed_color(attributex[currattribute, p], attributey[currattribute, p], attribute[currattribute, p],atrscale,atrscale,0,atrcolour,atrcolour,atrcolour,atrcolour,attributealpha[currattribute]);
	}
	if(waveaway == true)
	{
	shader_reset();	
	}
draw_sprite_ext(spr_selectcross,0,selectcrossx,selectcrossy,3,3,0,c_white,1);

}
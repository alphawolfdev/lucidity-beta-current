{
    "id": "650854bf-dbe9-4d29-9dcf-59042db98c78",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_namechoosemanager",
    "eventList": [
        {
            "id": "132589fc-4ff4-4604-8fa4-c0280e329e40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "650854bf-dbe9-4d29-9dcf-59042db98c78"
        },
        {
            "id": "f5aafee5-8d68-44d4-912d-95490e534465",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "650854bf-dbe9-4d29-9dcf-59042db98c78"
        },
        {
            "id": "d8ce182c-9188-4691-909e-4721968a861e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "650854bf-dbe9-4d29-9dcf-59042db98c78"
        },
        {
            "id": "8dd8e0f8-cff9-4dc5-bcff-e66812876f03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "650854bf-dbe9-4d29-9dcf-59042db98c78"
        },
        {
            "id": "81ec26c0-0455-4c4d-a307-5995ba592493",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "650854bf-dbe9-4d29-9dcf-59042db98c78"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
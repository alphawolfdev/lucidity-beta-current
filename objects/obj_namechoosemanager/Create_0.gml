/// @description Insert description here
// You can write your code in this editor
faded_in = false;
audioplayer = audio_emitter_create();
gaino = 0;
audio_emitter_gain(audioplayer,0.0);
audio_play_sound_on(audioplayer,mus_nameselect,true,0);
confirmalpha = 0;
waveaway = false;
waveTime = shader_get_uniform(shd_increasingwave,"Time");
addedspeed = 0;
atrscale = 1;
timepassed = 0;
atrcolour = c_aqua;
attributewidth[0, 0] = 0;
attributeheight[0, 0] = 0;
attributequestion[0] = "An Identity is not decided by a name alone. WHO are you?";
attributequestion[1] = "Motivation. What drives you,enables you to continue?Why are you here?";

attributealpha[0] = 0;
attribute[0, 0] = "A Hero";
attribute[0, 1] = "A Soldier";
attribute[0, 2] = "A Warrior";
attribute[0, 3] = "A Legend";
attribute[0, 4] = "A Human";
attributex[0, 0] = 30;
attributex[0, 1] = 480;
attributex[0, 2] = 220;
attributex[0, 3] = 30;
attributex[0, 4] = 480;
attributey[0, 0] = 320;
attributey[0, 1] = 320;
attributey[0, 2] = 380;
attributey[0, 3] = 440;
attributey[0, 4] = 440;
attributequestion[2] = "What do you view as most important?";

attributealpha[0] = 0;
attribute[2, 0] = "Justice";
attribute[2, 1] = "Honesty";
attribute[2, 2] = "Willpower";
attribute[2, 3] = "Ideals";
attribute[2, 4] = "Kindness";
attributex[2, 0] = 30;
attributex[2, 1] = 480;
attributex[2, 2] = 220;
attributex[2, 3] = 30;
attributex[2, 4] = 480;
attributey[2, 0] = 320;
attributey[2, 1] = 320;
attributey[2, 2] = 380;
attributey[2, 3] = 440;
attributey[2, 4] = 440;
attributequestion[3] = "What do you wish for most?";

attributealpha[0] = 0;
attribute[3, 0] = "A Quiet Life";
attribute[3, 1] = "Immortality";
attribute[3, 2] = "Skill";
attribute[3, 3] = "Knowledge";
attribute[3, 4] = "Riches";
attributex[3, 0] = 30;
attributex[3, 1] = 440;
attributex[3, 2] = 220;
attributex[3, 3] = 30;
attributex[3, 4] = 440;
attributey[3, 0] = 320;
attributey[3, 1] = 320;
attributey[3, 2] = 380;
attributey[3, 3] = 440;
attributey[3, 4] = 440;
attributequestion[4] = "Would you descrbe yourself as:";

attributealpha[0] = 0;
attribute[4, 0] = "Intelligent";
attribute[4, 1] = "Fast";
attribute[4, 2] = "Strong";
attribute[4, 3] = "Skilled";
attribute[4, 4] = "Determined";
attributex[4, 0] = 30;
attributex[4, 1] = 440;
attributex[4, 2] = 220;
attributex[4, 3] = 30;
attributex[4, 4] = 440;
attributey[4, 0] = 320;
attributey[4, 1] = 320;
attributey[4, 2] = 380;
attributey[4, 3] = 440;
attributey[4, 4] = 440;
attributestransout[0] = false;
attributequestion[5] = "Right now. Are you awake?";

attributealpha[0] = 0;
attribute[5, 0] = "I'm Lucid";
attribute[5, 1] = "I'm Lucid";
attribute[5, 2] = "I'm Lucid";
attribute[5, 3] = "I'm Lucid";
attribute[5, 4] = "I'm Lucid";
attributex[5, 0] = 30;
attributex[5, 1] = 450;
attributex[5, 2] = 220;
attributex[5, 3] = 30;
attributex[5, 4] = 450;
attributey[5, 0] = 320;
attributey[5, 1] = 320;
attributey[5, 2] = 380;
attributey[5, 3] = 440;
attributey[5, 4] = 440;
attribute[1, 0] = "Recognition";
attribute[1, 1] = "Fame";
attribute[1, 2] = "Strength";
attribute[1, 3] = "Power";
attribute[1, 4] = "No Idea";
attributex[1, 0] = 30;
attributex[1, 1] = 480;
attributex[1, 2] = 220;
attributex[1, 3] = 30;
attributex[1, 4] = 440;
attributey[1, 0] = 320;
attributey[1, 1] = 320;
attributey[1, 2] = 380;
attributey[1, 3] = 440;
attributey[1, 4] = 440;
selectedattribute = -8;
selectedattributestring[0] = "";
checkedforloop = false;
selectcrossx = room_width/2;
selectcrossy = room_height/2;
entercharattributes[0] = false;
charattributefade[0] = false;
attributestrans = false;
currattribute = 0;
alpha = 0;
question[0] = "First. Your identity. Who are you?";
letstring = "abcdefghijklmnopqrstuvwxyz";
capital = false;
space_inbetween = 30;
name_entered = false;
currnameletter = "";
name = "";
letterdraw = "";
name_length = 9
name_confirmed = false;
currnamelength = 0;
seedash = false;
alarm[0] = 1;
answer[0] = "";
maxletterow = room_width/36;
possible_letters = string_length(letstring);
numberofletters = 0;
for(i=0;i<possible_letters;i++)
{
letter[i] = string_char_at(letstring,i+1);
numberofletters ++;
}
currquestion = 0;
col = c_white;
maxquestions = array_length_1d(question);
colour = c_white;
key[0] = "";
spacecolour = c_white;
backspacecolour = c_white;
selectcolour = c_white;
option_inbetween = 0;
extranamesize = 0;
confirmcolour = c_white;
upcolour = c_white;
option[0] = "YES";
option[1] = "NO";
option_selected = 0;
confirmtransition = false;
show_response = false;
responsealpha = 0;
namerotation = 0;
for(i=0;i<array_length_1d(letter);i++)
{
	key[i+1] = letter[i];
}
key[0] = "arrow";
key[possible_letters+1] = "space";
key[possible_letters + 2] = "backspace";
key[possible_letters + 3] = "confirm";
selectedkey = key[0];
selected = 0;
maxselected = array_length_1d(key);
//Step 1 Name






//Step 2 c0nfirmation
can_confirm = false;
nameextrax = 0;
tilt_left = false;
name_confirmed = false;
response = "";


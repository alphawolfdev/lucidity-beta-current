{
    "id": "be032c29-fe5c-42ff-a8e5-5607e2415a4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_uiballcreator",
    "eventList": [
        {
            "id": "5e047d38-e743-4511-b0f7-0699940fa0dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be032c29-fe5c-42ff-a8e5-5607e2415a4f"
        },
        {
            "id": "fa410992-c645-49fc-8ac9-e50a8ccc85fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be032c29-fe5c-42ff-a8e5-5607e2415a4f"
        },
        {
            "id": "1d625b41-5f05-4ff4-8d3c-03aa09266a14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "be032c29-fe5c-42ff-a8e5-5607e2415a4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
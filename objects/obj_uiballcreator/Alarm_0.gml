/// @description Insert description here
// You can write your code in this editor
boru = instance_create_depth(random_range(20,620),random_range(8,400),1,obj_uiball);

boru.startsize = random_range(0.3,0.5);
boru.currsize = boru.startsize;
boru.maxgrowsize = boru.startsize* 1.5;
boru.decayrate = random_range(0.008,0.015);
boru.speedup = random_range(1,1.8);
boru.growspeed = random_range(0.005,0.01);
boru.curralpha = random_range(0.2,0.5);
alarmo = false;

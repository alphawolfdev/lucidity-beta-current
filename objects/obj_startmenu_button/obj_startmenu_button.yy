{
    "id": "9c006d43-4b50-4ced-b68f-4f8ba94af2e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_startmenu_button",
    "eventList": [
        {
            "id": "fbab122f-82c0-4e54-8f6c-a86f7ca65418",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9c006d43-4b50-4ced-b68f-4f8ba94af2e5"
        },
        {
            "id": "a09fc593-ed75-4eae-a46f-c20cff1471ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9c006d43-4b50-4ced-b68f-4f8ba94af2e5"
        },
        {
            "id": "3a10ea51-a9a4-4f8d-bf41-22aaa3d27971",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9c006d43-4b50-4ced-b68f-4f8ba94af2e5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
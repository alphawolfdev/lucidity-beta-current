{
    "id": "1daf8631-6784-458c-aba4-f6fd1ad32b18",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bigtext",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "VT323",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "15dc5750-02d1-4f6e-9d3f-925626a0ec61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 45,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "eeabedc8-e6fd-4d95-a957-356065240a1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 45,
                "offset": 7,
                "shift": 18,
                "w": 5,
                "x": 485,
                "y": 96
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ff3a29df-cca8-4aef-afcf-b2ebb8355077",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 194,
                "y": 96
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3ef3b8b2-94f6-4c35-9fbc-aca13b17cab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 362,
                "y": 49
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "beac3c75-e392-4da8-8c23-85525052186e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 380,
                "y": 49
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "46132670-b8e2-44a1-b313-9340adad772f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 272,
                "y": 49
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "eb87085e-b9c6-40ee-b148-77bf0b6de8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 344,
                "y": 49
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "95852ae5-b8bc-4ca6-a4f9-c5b91791ed6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 45,
                "offset": 7,
                "shift": 18,
                "w": 5,
                "x": 478,
                "y": 96
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b499543c-94f3-46fe-8a6a-efe8179379ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 45,
                "offset": 6,
                "shift": 18,
                "w": 9,
                "x": 398,
                "y": 96
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ffc53b5b-5fb4-44b0-ad31-4c8a51fc4e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 45,
                "offset": 5,
                "shift": 18,
                "w": 8,
                "x": 409,
                "y": 96
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "34ed9816-660e-408b-a029-a08354125749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 449,
                "y": 49
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "862fcf65-368d-4f39-b4d4-004354028c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 98,
                "y": 96
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bbb235b2-c59a-442a-8406-04359fadbb6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 45,
                "offset": 7,
                "shift": 18,
                "w": 6,
                "x": 446,
                "y": 96
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1dec3910-1854-4a9a-83a8-c94fb16abd93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 45,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 374,
                "y": 96
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9815a79b-d90d-4bac-8a64-685a756ee144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 45,
                "offset": 7,
                "shift": 18,
                "w": 6,
                "x": 470,
                "y": 96
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f6eac54c-b7b4-431f-a75d-634202a97f3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 236,
                "y": 49
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cff5630d-de60-45d5-8313-0b55750bedb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 481,
                "y": 49
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "938fad2a-cc49-4746-a33b-e35187a26e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 250,
                "y": 96
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f0ad2f2d-ab36-4ab1-8464-eb9ef5f7a06e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 465,
                "y": 49
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7599870a-d909-4384-8fa8-b1bc7ba1b8cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 2,
                "y": 96
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b980140f-98a6-429e-bc0b-1dc04189c497",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 326,
                "y": 49
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fecad2a5-3c85-413e-b50d-fc05f8b2a4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 15,
                "x": 415,
                "y": 49
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "185dc835-b2d5-4807-9a6d-64f873c55c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 50,
                "y": 96
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fe6017d9-2471-480f-8bf8-0a4d4bc94ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 66,
                "y": 96
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7bacb47b-4c27-4768-83c2-ec6dea51083d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 82,
                "y": 96
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "195d06fb-682e-4433-99cf-bb857e35b2d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 34,
                "y": 96
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2fde880c-b6d6-4aa4-93c3-b0ee022f2ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 45,
                "offset": 7,
                "shift": 18,
                "w": 6,
                "x": 454,
                "y": 96
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ac9df5fd-3b07-4e31-bbcd-0513038560bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 45,
                "offset": 7,
                "shift": 18,
                "w": 6,
                "x": 462,
                "y": 96
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c36ac949-17a5-4422-a9e6-f4ecae046f81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 45,
                "offset": 4,
                "shift": 18,
                "w": 12,
                "x": 348,
                "y": 96
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3a72677e-c1a5-45a0-864e-5adefc6d79cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 334,
                "y": 96
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "af3d27d9-7e8b-4356-a55e-756bfb2e257a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 45,
                "offset": 4,
                "shift": 18,
                "w": 12,
                "x": 320,
                "y": 96
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c55decd3-edde-47fd-b3c9-bae7eb91e6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 432,
                "y": 49
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8fb3c298-7ea4-4bbc-b263-5d1a0efb0b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 308,
                "y": 49
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3c68e59c-7fee-4809-b687-c1b07fd30855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 290,
                "y": 49
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "aadaac45-f08a-4ca5-8f50-00f14cdfe0e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 254,
                "y": 49
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f3e5c157-bedc-4a3f-9974-5f6bc0a2dbbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 218,
                "y": 49
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "433449e0-c987-4022-a2d7-654679648c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 200,
                "y": 49
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a0443527-bc59-45b5-800b-e06d7617d1a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 306,
                "y": 96
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1cdafe9d-6206-499b-af0f-04b228a38831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 264,
                "y": 96
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d8e559c0-23ed-4d47-baea-71f94a5ab9a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 146,
                "y": 49
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7aaba1f3-f18d-4011-a1ec-83b0e382dc6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 310,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "43d8f992-77d2-4185-8272-1195892820ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 45,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 362,
                "y": 96
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "726ca478-adc3-4ab9-a0b5-1e710a51ffa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 398,
                "y": 49
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c22319fe-dc6e-454d-8673-642930634655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f209f220-1d6b-46ff-9ed4-b4c31e0d88f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 162,
                "y": 96
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e2d8317d-e2d8-4790-b3b8-8f495cf59941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "346cb529-ac01-4ae2-bdb8-51625f36c9e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "19d86d6e-3744-4b65-a709-6cead60189e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 164,
                "y": 49
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2ce7894a-9d32-40b9-874d-317ebd72d5e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4965cb72-1b05-4488-bb74-a8f5519306d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2439256e-b31c-4d70-b4a4-83a14cce51c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "edbc26a2-1976-4c7a-af8b-9b1ad240df09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ec530127-65ae-48ee-92bf-21212c09874a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9760fb69-b98d-4e2b-bc88-1e65c78e90ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "92c92dc6-3b2e-4d88-99d5-0a764c0e4d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5932d9cc-e5a6-4035-92da-391c6b9eea75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ea799729-1ec7-4c6c-a0bd-c835758d0f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0469429e-82eb-4c23-91b8-addf918e89cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "194e9172-41f8-4152-9889-30c4be8f8a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 178,
                "y": 96
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "af545ba5-ea72-4d54-93ee-6fa4e303ecd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 45,
                "offset": 6,
                "shift": 18,
                "w": 7,
                "x": 437,
                "y": 96
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5e42fbb0-043d-4e44-ac89-c1c9b32e55e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "63a06504-3337-45fc-9689-62eb5eba9eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 45,
                "offset": 5,
                "shift": 18,
                "w": 7,
                "x": 419,
                "y": 96
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "59469053-f96b-4256-b3b4-20ce0a6c81b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a5c75883-9b22-4240-afc9-541fe968431a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 292,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b0617ed1-0d24-476e-92e0-e5f6dca1d4b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 45,
                "offset": 5,
                "shift": 18,
                "w": 7,
                "x": 428,
                "y": 96
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "550c5356-ed33-4a7f-8ecc-e77577a553ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 328,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c186aac1-4149-40cf-ba7a-639220c3eac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 128,
                "y": 49
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "62d5d68f-20f2-4852-bdab-dd1be5aafabc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 346,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "dc393d16-c54f-4f0a-b1ce-aa1d836f5cbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 110,
                "y": 49
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6ac7985b-045d-4d46-9d0a-003784d85c35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 92,
                "y": 49
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a0b76a1f-0030-4eff-9f4a-c4dc15a1446d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 278,
                "y": 96
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b929e9e4-17e5-4475-bd23-a56c95600e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 74,
                "y": 49
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3e8f3f4a-9d3f-4f3b-92a4-e11bf1c3d06f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 56,
                "y": 49
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e5910779-fa44-47f9-8121-3eab697aeee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 236,
                "y": 96
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c6fe40df-7b20-41b8-bc0b-3369e19479df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 10,
                "x": 386,
                "y": 96
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3bba4a47-e082-48de-ae1d-e1ad510691f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 38,
                "y": 49
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c3a347d5-158f-46ba-a064-0ec97b3f1089",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 222,
                "y": 96
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b7cae61d-d79d-40a8-a4c9-0fc6763240c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 49
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c193c905-18de-40e7-b1bd-0b2583f7a59d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 49
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0a788328-f08d-4e13-8052-b77c0565d51e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 490,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "606d9af3-9f59-4704-9a45-fb2ab9909ed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 472,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "963d2880-b2d7-4f9d-896d-189a80a8a8b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "96c92061-afce-48aa-b496-effa085f944c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 436,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "45dd382e-b45b-40d4-ad74-a1b8a011211b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 18,
                "y": 96
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1477974b-17be-446e-9d11-71a9beca1a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 114,
                "y": 96
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "dd6e8106-fd12-4e0f-9374-df636ecd107c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 418,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "070b32a9-f375-462a-a19c-1843de0e7d15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 400,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f17220f7-6b34-4ad9-8cca-d5da56901459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 382,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e8bd748e-0691-4ec8-a322-26382b3a0451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 130,
                "y": 96
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "977740e1-6b4b-449f-8aef-03bbc82cc907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 364,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1b0a0f80-1240-4e40-9fb5-a36e45fa3fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 146,
                "y": 96
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0d4bf511-b3cd-4fef-a862-88fa7016c329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 208,
                "y": 96
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7175f21e-6fd8-4fa0-9301-54951edb2b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 45,
                "offset": 7,
                "shift": 18,
                "w": 5,
                "x": 492,
                "y": 96
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "828adf19-daaa-4c71-ab1b-136fe0a8af72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 45,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 292,
                "y": 96
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "66c73925-ad87-4a29-94fc-f3215a919ea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 182,
                "y": 49
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 34,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
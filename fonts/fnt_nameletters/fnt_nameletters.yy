{
    "id": "610df54f-16ca-4743-bbcd-d377caa4ecf5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_nameletters",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "VT323",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a63f1dd5-8ae5-40ce-9c28-8c854bb69bb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 53,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "455639ca-c008-4df8-a8bb-355ca651c517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 53,
                "offset": 8,
                "shift": 21,
                "w": 6,
                "x": 224,
                "y": 167
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a592a4c2-e6fd-47b8-b09a-b8521951b984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 384,
                "y": 112
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b0f662d3-1829-49b8-9db1-47cf5139123c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 112
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ac8ab4e5-4c02-4f8c-9cf5-360eb2e431a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 23,
                "y": 112
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a24e1377-e86e-4ae5-8d89-8bcb030da2c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 443,
                "y": 57
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ba4b36e4-a9f4-4881-92a7-bc937ac468fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 464,
                "y": 57
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d8866782-4b0c-404f-b49c-24722c463105",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 53,
                "offset": 8,
                "shift": 21,
                "w": 6,
                "x": 216,
                "y": 167
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6dcea848-21ee-49e6-98a0-3043e05e1813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 53,
                "offset": 7,
                "shift": 21,
                "w": 10,
                "x": 124,
                "y": 167
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "32b6a63d-9378-40c7-b1a4-d22fbdfa9dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 53,
                "offset": 6,
                "shift": 21,
                "w": 10,
                "x": 136,
                "y": 167
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ca5e3fde-4d9d-4c0b-bfe8-83b3021a58f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 142,
                "y": 112
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c72ed769-41b4-4250-bc9b-f5f632ed333c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 237,
                "y": 112
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ac866f36-d4df-49a5-9d58-fdb458d49a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 53,
                "offset": 8,
                "shift": 21,
                "w": 7,
                "x": 180,
                "y": 167
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "eb7a797c-2348-44f3-83e3-50beb9e61851",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 53,
                "offset": 5,
                "shift": 21,
                "w": 12,
                "x": 96,
                "y": 167
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1a229ba9-bc18-46bf-8c25-990ee9760743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 53,
                "offset": 8,
                "shift": 21,
                "w": 7,
                "x": 207,
                "y": 167
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2f73afc4-0781-4fe3-be07-3dccc99d2600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 359,
                "y": 57
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cd157ba8-d28c-40b0-87f7-0479c8dc7e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 15,
                "x": 367,
                "y": 112
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "936a65b0-778b-401d-9951-f0b04010d635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 448,
                "y": 112
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "123c4ca9-d895-4126-a9f3-d68f18f30c97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 104,
                "y": 112
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9126db68-9112-4862-8ccb-dcd966808fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 161,
                "y": 112
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9fc4db64-8fff-4a66-9822-3301b3b327f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 485,
                "y": 57
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0fb45847-7b36-4541-96de-65b0f513022c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 64,
                "y": 112
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d034b32c-2018-4057-b096-06e521933eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 313,
                "y": 112
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "cfb41b77-f4d0-4a89-a15b-77bf1646537a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 218,
                "y": 112
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "df936ea1-dc33-41ec-9a44-a15cbfe8ca97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 15,
                "x": 350,
                "y": 112
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "dcb2e151-3236-434d-aad5-0e632585ecad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 199,
                "y": 112
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "edd7f1c7-a618-4829-b9fa-a0dcda958251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 53,
                "offset": 8,
                "shift": 21,
                "w": 7,
                "x": 189,
                "y": 167
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4f765f55-fff8-4e9a-9797-977d2683fb4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 53,
                "offset": 8,
                "shift": 21,
                "w": 7,
                "x": 198,
                "y": 167
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2368973d-a9a2-454d-8cf0-c257f319b470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 66,
                "y": 167
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "003b8dc1-e76a-4b4c-a236-ec7f28993023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 50,
                "y": 167
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e8fd277e-1d63-4337-8e13-7d5399ea29b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 34,
                "y": 167
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a4655e27-9f1a-4450-96cd-1fc4fb4a9f69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 84,
                "y": 112
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b985a9da-c6ce-43ac-ab82-4c87beb657d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 422,
                "y": 57
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0be1138e-02fa-49df-9e65-e2a62ffd4dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 401,
                "y": 57
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "991515ef-58d5-45b3-954a-69fb16c3e6be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 380,
                "y": 57
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "74a8b6a2-e999-4c61-9508-ff20d654c671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 338,
                "y": 57
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "27798551-b435-46b5-aa92-8cdd0eb426af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 317,
                "y": 57
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6dafd0a6-e51e-4515-9472-e99898a6f204",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 18,
                "y": 167
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "88641525-54f9-468a-947e-23a64867f643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 464,
                "y": 112
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d0b2aec8-f7e2-4a06-9f34-7b318f8681ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 254,
                "y": 57
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ff1eda56-aac0-4f1b-93a9-131575309fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9b307c67-4a7b-4260-bd6f-93e1eb0f70ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 53,
                "offset": 5,
                "shift": 21,
                "w": 12,
                "x": 82,
                "y": 167
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "73594de5-0d09-469c-b05c-44d61d52754b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 44,
                "y": 112
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9c88de80-cabd-4a55-94d9-ff8ef62b6845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 319,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "00b1fcbe-61bb-470e-9946-d4dd5b0da661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 16,
                "x": 332,
                "y": 112
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "95cdbcfc-13bb-4a3e-82a4-d6e7191901ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 298,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fc09fc0b-dde5-4271-b299-feef2ebbff41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 277,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b4069b70-8237-4961-a87c-8bb85f65d4b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 275,
                "y": 57
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f1a5af26-a26d-4d3c-bd06-64f99884d0d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "97d7d3f1-3fb6-4d07-8f58-c2efa00a89a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9d40749e-5154-45d2-8fa4-dcd91e6b1916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "708e2e41-0a40-42bd-b364-0d389cea3859",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "977d55d5-84b9-4362-aad3-7ccee0a49901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7898156f-b2e7-4f85-9f08-9f22dd84371e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "aecf1f62-9e09-4e37-a5e7-520d894dd3e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f61e7dfe-c285-4282-936b-5281a9f83dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c1a345b0-4f6f-4170-b087-22d08bed69e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cb2489f8-a7cd-43db-9693-a3888499b037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b17b1e6c-0eba-41f6-a336-d046432a4e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 180,
                "y": 112
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "425c17e4-6d26-452b-acbe-f64fb729453a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 53,
                "offset": 7,
                "shift": 21,
                "w": 9,
                "x": 159,
                "y": 167
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "76b1dace-5681-4730-8b06-60da32cac4dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "25831e1f-3693-4522-8c80-d421bf9a6666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 53,
                "offset": 6,
                "shift": 21,
                "w": 9,
                "x": 148,
                "y": 167
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "46826774-0097-499c-b3b1-826b2fde80bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c4085747-4f51-40d0-85df-45b654be593b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 340,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c9ab32f3-a74c-49cf-bc6b-fdc4aea3648e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 53,
                "offset": 6,
                "shift": 21,
                "w": 8,
                "x": 170,
                "y": 167
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3b8dfedc-01ff-4cf3-8e75-6e7fb0f1a699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 382,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ad50373b-b71c-4478-aa0d-3f60b83b3e73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 233,
                "y": 57
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d3f27a45-efb8-466b-ade6-b16a17d24f6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 403,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a6fa383e-912e-42f1-b45b-371b6f2523c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 212,
                "y": 57
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "70e5e01b-fcc9-4331-8d29-d4d4961db746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 191,
                "y": 57
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fda9f265-bacb-4fec-a6b1-c4778122d68d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 480,
                "y": 112
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "016d1761-fe85-4ee8-9f03-524b82643383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 170,
                "y": 57
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e32de92b-ae4a-4497-943a-566f83b4567b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 149,
                "y": 57
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4000cddc-4a9b-4d29-9743-0d3f111db9c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 432,
                "y": 112
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b35855ee-eb67-44ad-b0d5-5b0f3449021e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 12,
                "x": 110,
                "y": 167
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ef05dacc-46f8-4611-baff-bfe0b2c151fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 128,
                "y": 57
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c98cff4b-0b53-4f90-bdbc-bbfa234856b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 416,
                "y": 112
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9fed8838-7d15-4e60-8645-4b3083e1b1a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 107,
                "y": 57
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6201ba5d-782b-4c65-8097-2a7f8233f5e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 86,
                "y": 57
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d8172d45-ddd0-4956-a45d-5f2a634c9b1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 65,
                "y": 57
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b14590a0-a140-4f70-b2f6-0d3d6de97f2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 44,
                "y": 57
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "424bcd31-6878-4a66-ae34-a7ca836b533e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 23,
                "y": 57
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6a9fd1d6-5f05-4400-b68b-b9b10c68e7fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 57
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "865329ac-bc7d-4b64-9db1-edc885103e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 123,
                "y": 112
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f63311dd-f1ab-4288-998b-569e3b635b7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 256,
                "y": 112
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b1902d23-ffea-4100-ada5-90a37e2d8023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 487,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "be8856ee-ca24-4dfb-bef1-18fc45b89df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 466,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "734252bb-ffe3-4baa-ae99-f2763cb9eafa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "baae31ff-6717-4668-91f7-22a48ff2e24b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 275,
                "y": 112
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7ca0fe5c-fa6c-4982-afca-035894cad749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 53,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "5dc632cb-5f68-446e-9fa6-19a5412e12c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 53,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 294,
                "y": 112
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4b54ed1b-3f23-4b18-9295-5840461b7e7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 400,
                "y": 112
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "90d97abf-ad78-4465-b9c5-49ef128d90c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 53,
                "offset": 8,
                "shift": 21,
                "w": 6,
                "x": 232,
                "y": 167
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ab64160f-f29d-4556-b0a0-d414eaee490a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 53,
                "offset": 4,
                "shift": 21,
                "w": 14,
                "x": 2,
                "y": 167
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6aa1a528-35d9-4e7f-8025-100d1d7ff129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 53,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 296,
                "y": 57
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 40,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
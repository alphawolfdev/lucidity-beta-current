///scr_saveGame();
//scr_saveGame();
var baseList = ds_list_create();

var dataMap = ds_map_create();
ds_list_add(baseList,dataMap);
ds_list_mark_as_map(baseList,ds_list_size(baseList)-1);

//add vars to map;
//Player Main Data, Coords etc.
ds_map_add(dataMap,"playerxcoord",global.playerx);
ds_map_add(dataMap,"playerxcoord",global.playery);



//Actual saving
var wrapper = ds_map_create();
ds_map_add_list(wrapper,"BASE",baseList);

var saveString = json_encode(wrapper);

var filename = "save1.sav";
var _buffer = buffer_create(string_byte_length(saveString)+1, buffer_fixed, 1);
buffer_write(_buffer,buffer_string,saveString);
buffer_save(_buffer,filename);
buffer_delete(_buffer);

//get rid of data
ds_map_destroy(wrapper);


